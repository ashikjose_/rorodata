const labels = {
  en: {
    ENTER_USERNAME: "Enter Username",
    SUBMIT: "Submit",
  },

  hn: {
    ENTER_USERNAME: "उपयोगकर्ता नाम दर्ज करें",
    SUBMIT: "जमा करें",
  }
}

const getLabels = (lang) => {
  return labels[lang]
}

export default {
  getLabels
}
