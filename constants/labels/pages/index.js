const labels = {
  en: {
    REPOSITORIES: "Repositories",
    PER_PAGE: "Per page"
  },

  hn: {
    REPOSITORIES: "फ़ाइल",
    PER_PAGE: "प्रति पृष्ठ"
  }
}

const getLabels = (lang) => {
  return labels[lang]
}

export default {
  getLabels
}
