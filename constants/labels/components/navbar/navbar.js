

const labels = {
  en: {
    LOGOUT: "Logout",
    NAME: "Github API"
  },

  hn: {
    LOGOUT: "लोग आउट",
    NAME: "Github API"
  }
}

const getLabels = (lang) => {

  return labels[lang]
}

export default {
  getLabels
}
