const labels = {
  en: {
    FOLLOWERS: "Followers",
    FOLLOWING: "Following",
    CREATED: "Created",
    LAST_UPDATED: "Last Updated"
  },

  hn: {
    FOLLOWERS: "समर्थक",
    FOLLOWING: "निम्नलिखित",
    CREATED: "बनाया था",
    LAST_UPDATED: "आखरी अपडेट"
  }
}

const getLabels = (lang) => {

  return labels[lang]
}

export default {
  getLabels
}
