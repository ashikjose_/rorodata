const baseUrl = "https://api.github.com"

const apiUrl = {
  login: "/users/",
}

const getUrl = url => {
  return baseUrl + apiUrl[url]
}

export default {
  getUrl,
}
