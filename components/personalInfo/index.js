import moment from 'moment'
import LABELS from '../../constants/labels/components/personalInfo/personalInfo';
import STORAGE from '../../libs/storage';
import { get } from 'http';

export default {
  props: ['data'],
  computed: {
    /**
     * Function to retreive Labels based on selected lang
     */
    labels() {
      return LABELS.getLabels(STORAGE.getSessionStorage('lang'))
    },
    /**
     * Function to set created_at
     */
    created_at() {
      return moment(this.data.created_at).format('MMMM Do YYYY')
    }, 
    /**
     * Function to set updated_at
     */
    updated_at() {
      return moment(this.data.updated_at).format('MMMM Do YYYY')
    }
  },
  data() {
    return {
    }
  },
  created() {
  },
  components: {
  },
  methods: {

  }
}