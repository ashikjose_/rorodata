import { mount, createLocalVue } from '@vue/test-utils'
import Component from './index.vue'
import BootstrapVue from 'bootstrap-vue'

const localVue = createLocalVue()
localVue.use(BootstrapVue)

describe('Component', () => {
  test('is a Vue instance', () => {
    const wrapper = mount(Component , {localVue})
    expect(wrapper.isVueInstance()).toBeTruthy()
  })
})
