import STORAGE from "../../libs/storage.js"

export default {
  props: {
    type: {
      type: Number,
      default: () => {
        return 1
      }
    }
  },
  computed: {
    /**
     * Function Retreive Labels Based on the selected Lang
     */
    lang() {
      return STORAGE.getLocalStorage("lang")
    },
  },
  data() {
    return {
    }
  },
  created() {
  },
  components: {
  },
  methods: {
    /**
     * Function to handle Logout
     */
    logout() {
      this.$store.dispatch("auth/logout")
      this.$router.push({path: '/'})
    },

    /**
     * Fnction to select a language which gets dispatched to store/common
     * @param {String} lang 
     */
    select_lang(lang) {
      this.$store.dispatch("common/set_lang", {'lang': lang})
      this.$router.push({path: '/'})
    }
  }
}