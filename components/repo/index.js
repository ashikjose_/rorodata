import moment from 'moment'
import LABELS from '../../constants/labels/components/repo';

export default {
  props: ['name', 'description', 'full_name', 'url', 'fetch'],
  computed: {
    /**
     * Function Retreive Labels Based on the selected Lang
     */
    labels () {
      return LABELS.getLabels('en')
    },
  },
  data() {
    return {
    }
  },
  created() {

  },
  components: {
  },
  methods: {

  }
}