/**
 * Import Modules
 * api -> Backend api URL'S
 * storage -> Functions for storing to session storage
 */
import API from "@/libs/api"
// import cookies from 'js-cookie'
import STORAGE from "@/libs/storage"

const state = () => ({
  user: null
})

const mutations = {
  login: (state, payload) => {
    state.user = payload.login
    STORAGE.updateSessionStorage("user", payload.login)
    STORAGE.updateLocalStorage("user", payload.login)
    if(!STORAGE.getSessionStorage("lang")) {
      STORAGE.updateSessionStorage("lang", 'en')
      STORAGE.updateLocalStorage("lang", 'en')
    }
  },
  /**
   * Logout Mutation
   * Sets the login state to false
   * Sets the toke to null
   * Removes the existing values from session storage
   */
  logout: (state) => {
    state.user = null
    STORAGE.deleteSessionStorage()
    STORAGE.deleteLocalStorage()
  },
}

const actions = {
  /**
   * Login Action
   * Sends a post request to /users/{user}
   * Payload will contain username and password
   * Calls the Login Mutation
   */
  login: (context, payload) => {
    return new Promise((resolve, reject) => {
      API.get("login", payload).then(
        data => {
          context.commit("login", data)
          resolve(data)
        },
        err => {
          reject(err)
        }
      )
    })
  },
  /**
   * Logout Action
   * Sends a post request to /account/logout
   * Calls the Logout Mutation
   * Payload contains a method which routes to root page
   */
  logout: (context, payload) => {
    context.commit("logout")
  },
}

export default {
  namespaced: true,
  actions,
  mutations,
  state,
}
