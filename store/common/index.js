
import API from "@/libs/api"
import STORAGE from "@/libs/storage"

const state = () => ({
  
})

const mutations = {
  
}

const actions = {
  url_fetch: (context, payload) => {
    return new Promise((resolve, reject) => {
      API.get_custom_url(payload.url).then(
        data => {
          resolve(data)
        },
        err => {
          reject(err)
        }
      )
    })
  },
  url_fetch_repo: (context, payload) => {
    return new Promise((resolve, reject) => {
      API.get_custom_url_repo(payload.url, payload.page, payload.per_page).then(
        data => {
          resolve(data)
        },
        err => {
          reject(err)
        }
      )
    })
  },
  set_lang: (context, payload) => {
    STORAGE.updateSessionStorage("lang", payload.lang)
    STORAGE.updateLocalStorage("lang", payload.lang)
  }
}

export default {
  namespaced: true,
  actions,
  mutations,
  state,
}
