import axios from "axios"
import APIURL from '@/constants/api/apiUrl'

const get_custom_url = (url) => {
  return new Promise((resolve, reject) => {
    axios
      .get(url)
      .then(response => {
        resolve(response.data)
      })
      .catch(error => {
        reject(error)
      })
  })
}

const get = (url, data) => {
  return new Promise((resolve, reject) => {
    axios
      .get(APIURL.getUrl(url) + `${data.user}`)
      .then(response => {
        resolve(response.data)
      })
      .catch(error => {
        reject(error)
      })
  })
}

const get_custom_url_repo = (url, page, per_page) => {
  const finalURL = url + '?page=' + `${page}` + '&per_page=' + `${per_page}`
  return new Promise((resolve, reject) => {
    axios
      .get(finalURL)
      .then(response => {
        resolve(response.data)
      })
      .catch(error => {
        reject(error)
      })
  })
}

export default {
  get,
  get_custom_url,
  get_custom_url_repo
}