import STORAGE from "../../libs/storage"
import moment from 'moment'
import personalInfo from '../../components/personalInfo/index.vue'
import repo from '../../components/repo/index.vue'
import LABELS from '../../constants/labels/pages/index.js';

export default {
  data() {
    return {
      id: null,
      user: null,
      data: null,
      created_at: null,
      updated_at: null,
      flagUrl: null,
      repos: null,

      // Pagination Data
      per_page_array: [
        5, 10, 25, 50
      ],
      repos_count: null,
      per_page: 5,
      page: 1, 

    }
  },
  computed: {
    /**
     * Function to retreive Labels based on selected lang
     */
    labels() {
      return LABELS.getLabels(STORAGE.getSessionStorage('lang'))
    },
  },
  created() {
    if(STORAGE.getLocalStorage('user')) {
      this.user = STORAGE.getLocalStorage('user')
    } else {
      this.user = this.$route.params.id
    }
    this.fetch_user_data()
  },
  components: {
    personalInfo,
    repo
  },
  methods: {
    /**
     * Function to get the user data
     * and then call the user repo list
     */
    fetch_user_data() {
      this.$store.dispatch("auth/login", {'user': this.user}).then(
        (data) => {
          this.data = data
          this.fetch_repo_count()
          this.fetch_user_repos()
        },
        err => {
          this.error = err.message
        }
      )
    },

    /**
     * Function to get the initial list of content repos
     */
    fetch_user_repos() {
      this.$store.dispatch("common/url_fetch_repo", {'url': this.data.repos_url, 'page': this.page, 'per_page': this.per_page}).then(
        (data) => {
          this.repos = data
        },
        err => {
          this.error = err.message
        }
      )
    },
    
    /**
     * Function to retreive and store total repo count
     */
    fetch_repo_count() {
      this.$store.dispatch("common/url_fetch", {'url': this.data.repos_url}).then(
        (data) => {
          this.repos_count = data.length
        },
        err => {
          this.error = err.message
        }
      )
    },

    /**
     * Function to handling contents of repos
     * @param {String} repo_url 
     * @param {String} name 
     */
    fetch_repo_contents(repo_url, name) {
      this.$router.push({name: 'user-repos-id' , params: {id: name, url: repo_url + "/contents/" }})
    },

    /**
     * Function to handle the selection of repos to be shown per page
     * @param {Number} number 
     */
    select_per_page(number) {
      this.per_page = number
      this.page = 1
      this.fetch_user_repos()
    },

    /**
     * Function to handle fetching the next set of repos from change of page number
     * @param {Number} page 
     */
    handle_page_change(page) {
      this.repos = []
      this.page = page
      this.fetch_user_repos()
    }
  },
  layout: 'default'
}