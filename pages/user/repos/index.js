import STORAGE from "../../../libs/storage"
import moment from 'moment'

export default {
  data() {
    return {
      repo_content_list: null,
      user: null,
      dir: [], 
      file_content: null
    }
  },
  created() {
    this.user = STORAGE.getSessionStorage('user')
    //console.log(this.$route.params)
    if(this.$route.params.url) {
      this.$store.dispatch("common/url_fetch", {'url': this.$route.params.url}).then(
        (data) => {
          this.repo_content_list = data
        },
        err => {
          this.error = err.message
        }
      )
    } else {
      this.$router.push({path: '/'})
    }
    
  },
  computed: {
    /**
     * Function to sort the repo list
     */
    sortedFiles: function() {
      if (this.repo_content_list) {
        return this.repo_content_list.slice(0).sort(function(a, b) {
          if (a.type !== b.type) {
              if (a.type === 'dir') {
                  return -1;
              } else {
                  return 1;
              }
          } else {
              if (a.name < b.name) {
                  return -1;
              } else {
                  return 1;
              }
          }
        });

      }
      
    }
  },
  components: {
  },
  methods: {
    /**
     * Function to go back to user Page
     */
    go_to_user_page() {
      this.$router.push({path: '/user/' + this.user})
    },

    /**
     * Function to retreive data from dir 
     */
    retreive_content() {
      this.$store.dispatch("common/url_fetch", {'url': this.$route.params.url}).then(
        (data) => {
          this.repo_content_list = data
          this.dir = []
        },
        err => {
          this.error = err.message
        }
      )
    },

    /**
     * Function to retreive data from dir 
     */
    retreive_dir_content(dir_url) {
      this.$store.dispatch("common/url_fetch", {'url': dir_url}).then(
        (data) => {
          this.repo_content_list = data
        },
        err => {
          this.error = err.message
        }
      )
    },

    /**
     * Function to go back in dir tree
     */
    retreive_dir_content_from_loaded(dir_url, index) {
      this.$store.dispatch("common/url_fetch", {'url': dir_url}).then(
        (data) => {
          this.repo_content_list = data
          
          let new_arr = this.dir.splice(0, index+1)
          this.dir = new_arr
          
        },
        err => {
          this.error = err.message
        }
      )
    },

    /**
     * Function to download the contents of the file
     * @param {Object} item 
     */
    download_file_content(item) {
      this.$store.dispatch("common/url_fetch", {'url': item.download_url})
      .then((response) => {
        /*const url = window.URL.createObjectURL(new Blob([response]));
        const link = document.createElement('a');
        link.href = url;
        link.setAttribute('download', item.name); //or any other extension
        document.body.appendChild(link);
        link.click();*/
        this.file_content = response
      })
    },

    /**
     * Function to download the contents of the file
     * @param {Object} item 
     */
    download_file(item) {
      this.$store.dispatch("common/url_fetch", {'url': item.download_url})
      .then((response) => {
        const url = window.URL.createObjectURL(new Blob([response]));
        const link = document.createElement('a');
        link.href = url;
        link.setAttribute('download', item.name); //or any other extension
        document.body.appendChild(link);
        link.click();
      })
    },

    /**
     * Function to add directory to the tree array 
     * @param {Object} item 
     */
    add_dir(item) {
      this.file_content = null
      let count = this.dir.length
      this.dir[this.dir.length] = { name: item.name, url: item.url }
      this.repo_content_list = []
      this.retreive_dir_content(this.dir[count].url)
    }
  },
  layout: 'default'
}