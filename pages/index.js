import STORAGE from "../libs/storage"
import LABELS from '../constants/labels/pages/landing/landing'

export default {
  data() {
    return {
      username: null,
    }
  },
  computed: {
    /**
     * Function Retreive Labels Based on the selected Lang
     */
    labels() {
      return LABELS.getLabels('en')
    },
  },
  components: {
  },
  created() {
    if(STORAGE.getLocalStorage('user'))
      this.$router.push({path: '/user/' + STORAGE.getLocalStorage('user')})
  },
  methods: {
    /**
     * Function to handle the fetching of user data after the 
     * username is entered in to the field
     * @param {Object} evt 
     */
    submit(evt) {
      evt.preventDefault();
      this.$store.dispatch("auth/login", {'user': this.username}).then(
        (data) => {
          this.$router.push({path: '/user/' + this.username})
        },
        err => {
          this.error = err.message
        }
      )
    }
  },
  layout: 'landing'
}