import Vue from "vue"

Vue.filter('shorten', (value) => {
  if (value != undefined) {
    if(value.length >= 18){
      let first = value.substring(0,14)
      let filler = "..."
      let last = value.substring(value.length-3, value.length)
      return first + filler + last
    }
    else {
      return value
    }   
  }
})

Vue.filter('shorten20', (value) => {
  if (value != undefined) {
    if(value.length >= 20){
      let first = value.substring(0,15)
      let filler = "..."
      let last = value.substring(value.length-5, value.length)
      return first + filler + last
    }
    else {
      return value
    }   
  }
})

Vue.filter('shorten50', (value) => {
  if (value != undefined) {
    if(value.length >= 50){
      let first = value.substring(0,45)
      let filler = "... "
      let last = value.substring(value.length-3, value.length)
      return first + filler + last
    }
    else {
      return value
    }   
  }
})
